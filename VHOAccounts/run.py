from VHOaccounts import CreateVHOAccounts

def main():
    csvFilename = "test_colors.csv" #Adjust name of csv file to be generated
    passwords = "colors.txt" #choose theme of passwords you want to generate --> all options can be found in "password_themes" folder: S:\DES\00_in_Bearbeitung\VHOAccounts\password_themes
    startnumber = 205 #adjust number where VHO accounts ramge should start from
    endnumber = 300 #adjust number where VHO accounts range should end --> new created accounts = endnumber-startnumber-1
    description = "Ersatzaccount" #adjust descripton --> optional: if you don't want to add a discription don't add it as argument int generateCSVFile()
    
    json_data = CreateVHOAccounts().createJSON(passwords, startnumber, endnumber, description)
    CreateVHOAccounts().generateCSVFile(json_data, csvFilename)


if __name__ == "__main__":
    main()