from datetime import date
import pandas as pd
import json
from io import StringIO
import os

class CreateVHOAccounts(): 
    def __init__(self) -> None:
        self.csvheader = ["username", "password", "givenname", "surname", "mail", "description", "dateExpire"]
        self.username = "net-ersatz.temp."
        self.givenname = "Ersatz"
        self.surname = "Account"
        self.mail ="digital-exams@id.ethz.ch"
        self.json_data = []
        self.DateExpire = date.today().replace(year=date.today().year+5).strftime("%d.%m.%Y") #current date + 2 years

    def __generatePasswordList(self, wordFile: str):
        """Generates a list of words from textfile
        Parameters: 
            - wordFile (string): 
        Returns: 
            - list: with strings
        """
        with open(os.path.join(r"S:\DES\00_in_Bearbeitung\VHOAccounts\password_themes", wordFile), "r") as file:
            line = file.readline()
            wordList = line.split(",")
            wordList = list(set(wordList))
        return wordList
    
    def createJSON(self, wordFile: str, startNum: int, endNum: int, description: str = None) -> json:
        """
        Creates a json with username, password, givenname, surname, mail, description, dateExpire from a wordlist and a range of numbers
        Parameters:
            - wordFile (string): takes a txt file with words to generate the password
            - startNum (int): first number in the list of VHO Account names 
            - endNum (int): last number (+1) in the list of VHO Account names 
            - description (string): desciption is optional
        """
        data = []
        numbers = [f"{num:03d}" for num in range(startNum,endNum)]
        for word, num in zip(self.__generatePasswordList(wordFile), numbers):
            data.append({"ID": num, "username": self.username+num, "password": word+num, "givenname": self.givenname+num, "surname": self.surname, "mail": self.mail, "description": description, "dateExpire":self.DateExpire})
        json_data = json.dumps(data)
        return json_data

    def generateCSVFile(self, json_data: str, csvFilename: str):
        """
        Generates a csv file with substitute VHO Accounts.
        Parameters:
            - json_data (string): json with id, username, password, givenname, surname, mail, description, dateExpire
            - csvFilename (string): filename of the csv to be created
        Returns:
            - csv file: with 7 columns (username, password, givenname, surname, mail, description, dateExpire)
        """
        json_data = StringIO(json_data)
        df = pd.read_json(json_data)
        df = df.drop(columns=[df.columns[0]]) #remove id column
        output_folder = "csv_outputs/"
        os.makedirs(os.path.join(r"S:\DES\00_in_Bearbeitung\VHOAccounts", output_folder), exist_ok=True) #check if there is an output folder and if not --> create one 
        df.to_csv(os.path.join(r"S:\DES\00_in_Bearbeitung\VHOAccounts", output_folder, csvFilename), index=False)