from SP_connection import SPconnection
from ChecklistenMails import ChecklistenMail

def main():
    SPdata = SPconnection().get_data_from_Termine()
    ChecklistenMail(SPdata).generate_mail(semester="2024HS", form="Semesterend, Benotete SL")
    #Benotete SL, Benotete SL (w), Session, Semesterend, Semesterend (W), Unbenotete SL, Unbenotete SL (W), Leistungselement, Probe

if __name__ == "__main__":
    main()