from SP_connection import SPconnection
import win32com.client
import openpyxl
import os
from jinja2 import Environment, FileSystemLoader
from datetime import datetime, timedelta
import pandas as pd 


""" SPConnection= SPConnection() """

#SPdata = SPconnection().get_data_from_Termine()


class ChecklistenMail: 

    def __init__(self, SPdata) -> None:
       self.outlook = win32com.client.Dispatch("Outlook.Application")
       self.SPdata = SPdata
       self.recipients = []
       self.planungsgruppen_dict = {}
       self.planungsgruppen_generated = []
       self.data_for_excel = {}
       self.already_generated = False
       #self.env = Environment(loader=FileSystemLoader(searchpath='./'))
       #self.env = Environment(loader=FileSystemLoader(searchpath=os.path.join(os.getcwd(), 'inputs')))
       self.env = Environment(loader=FileSystemLoader(searchpath=r"S:\DES\00_in_Bearbeitung\PythonScript_ChecklistenMails_SZ\inputs"))
       self.outlook = win32com.client.Dispatch("Outlook.Application")


    def print_data(self):
        '''
        prints all the data which was fetch from SPdata instance
        '''
        for item in self.SPdata:
            if item["Title"]=="376-1721-00S":
                print("---------------------------------------\n", item)


    def __check_planungsgruppe(self):
        """
        loops through all given items and checks for Planungsgruppen. 
        Createas an item with the name of the planungsgruppe as key and a list of the according exams (tuples of Lk-Titel, Lk-Nummer and Link) as value in the dict self.planungsgruppen_dict
        """
        for item in self.SPdata: #loop through items from SP
            if "Planungsgruppe" in item: #check if there is a Planungsgruppe for this exam
                if item["Planungsgruppe"] not in self.planungsgruppen_dict: #check if this planungsgruppe is already in dict 
                    self.planungsgruppen_dict[item["Planungsgruppe"]] = [] #if not --> create no item with empty list
                    try:
                        self.planungsgruppen_dict[item["Planungsgruppe"]].append((item["LKTitle"], item["Title"], item["Pruefungslink"]["Url"]))
                    except KeyError as er: #in case there is no Link available 
                        pass
                else: # if yes, add exam to the list of according planungsgruppe
                    try:
                        self.planungsgruppen_dict[item["Planungsgruppe"]].append((item["LKTitle"], item["Title"], item["Pruefungslink"]["Url"]))
                    except KeyError as er:
                        #print("CAUTION: could not find link for: ", item["Title"])
                        pass
            else: 
                continue


    def __create_msg_file(self, semester:str, html_content:str, already_generated: bool, subject:str, filename: str, form:str):
        """
        Generates a .msg file out of given html_content 
        Parameters:
            - semester (string): the defined semester
            - html_content (string): predefined html content
            - already_generated (bool): TRUE if mail was already generated
            - subject (string): mail subject (header)
            - filename (string): mail filename 
            - form (string): Prüfungsform --> multiple possible (comma seperated)
        """
        if not already_generated: #checks if mail already generated due to planungsgruppen
            msg = self.outlook.CreateItem(0) # Create a new email item
            msg.Subject = subject
            msg.HTMLBody = html_content 
            msg.To = "; ".join(self.recipients) #join email adresse from list
            filename = filename+".msg"
            #output_folder = "output/"+form+semester
            output_folder = os.path.join(r"S:\DES\00_in_Bearbeitung\PythonScript_ChecklistenMails_SZ\outputs", semester, form)
            os.makedirs(output_folder, exist_ok=True) #check if there is an output folder and if not --> create one 
            msg.SaveAs(os.path.join(os.getcwd(), output_folder, filename))  # Save the email as a .msg file       


    def generate_mail(self, semester:str, form:str):
        """
        Generates the actual checklistenmail. 
        Parameters:
            - semester (string): the defined semester
            - form (string): Prüfungsform
        calls:
            - __create_msg_file 
            - __generate_excel
            - __check_planungsgruppe
        """
        print("Checklistenmails für ", form, semester, " werden generiert.")
        
        self.__check_planungsgruppe()

        for item in self.SPdata: #loop through items from SP
            self.recipients = [] #empty list with recipients 
            data = {} #initialize dict for placeholder values in mail body

            #check Prüfungsform and semester
            if item["Pruefungsform"] in form and item["Semester"]==semester:
                LKnummer = item["Title"] #set LK-Nummer to current LK-Nummer
                LKTitle = item["LKTitle"] #set LK-Titel to current LK-Titel 
                self.data_for_excel[item["id"]] = {"LK-Nummer": LKnummer, "LK-Titel": LKTitle}

                #check Date
                if 'Datum' in item and item["Datum"]!= "": #retrieve date if exists and not null
                    date_str = item["Datum"]
                    try:
                        date_obj = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%SZ") #check if date is in the right format
                    except ValueError as e:
                        print(f"Error parsing date: {e} for {date_str}") #print value error if its not in the right format
                        raise
                    """ date_obj = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%SZ") """
                    #date_obj
                    date_only = date_obj.date()# Extract the date part (just the date, no time)
                    ExamDate = date_only.strftime("%d.%m.%Y") # Format the new date to "DD.MM.YYYY"
                    date_filename = date_only.strftime("%Y%m%d")
                    print(date_filename)
                    #Date one week before exam --> send link to us
                    #one_week = timedelta(weeks=1) #define one week 
                    DateMin1w =  (date_only - timedelta(weeks=1)).strftime("%d.%m.%Y") # Subtract one week (7 days) from the date and Format the new date to "DD.MM.YYYY"              
                    DateMin2w = (date_only - timedelta(weeks=2)).strftime("%d.%m.%Y")
                    DateMin5w = (date_only - timedelta(weeks=5)).strftime("%d.%m.%Y")
                    DateMin6w = (date_only - timedelta(weeks=6)).strftime("%d.%m.%Y")
                    self.data_for_excel[item["id"]]["Exam date"]= ExamDate
                else:
                    try:
                        print("CAUTION: No DATES for " +LKnummer +": "+item["TerminStatus"]) #if there is no date or empty date in SP
                        ExamDate = ""
                        self.data_for_excel[item["id"]]["Exam date"] = "No exam date"
                        self.data_for_excel[item["id"]]["To do Termine"] = item["TerminStatus"] 
                        pass
                    except KeyError as e:
                        print("CAUTION: No DATES for " +LKnummer) #if there is no date or empty date in SP
                        ExamDate = ""
                        self.data_for_excel[item["id"]]["Exam date"] = "No exam date"
                        self.data_for_excel[item["id"]]["To do Termine"] = ""


                #check Prüfungslink
                if "Pruefungslink" in item: #check if there is a Prüfungslink
                    ExamLink = item["Pruefungslink"]["Url"]
                    self.data_for_excel[item["id"]]["Prüfungslink"] = ExamLink
                elif item["Setup"] == "Linux": #check if its a Linux exam as those don't have a moodle link
                    ExamLink = ""
                    self.data_for_excel[item["id"]]["Prüfungslink"] = "Kein Moodle Link, da Linux"
                    pass
                else:
                    try: 
                        print("CAUTION: No EXAM LINK for "+LKnummer +": "+item["TerminStatus"])
                        self.data_for_excel[item["id"]]["To do Termine"] = item["TerminStatus"] 
                    except KeyError as e:
                        self.data_for_excel[item["id"]]["To do Termine"] = ""
                        print("To DO TERMINE: ERROR: CAUTION: No EXAM LINK for "+LKnummer) #print message if the link is missing without "To do Termine"
                        pass
                    ExamLink = ""
                    self.data_for_excel[item["id"]]["Prüfungslink"] = "no Link"

                #Recipients 
                for examin in item["Examinator"]:
                    self.recipients.append(examin["Email"].strip())
                if "Assistant" in item: #check if there are assistants, as it isn't always the case
                    for assi in item["Assistant"]:
                        self.recipients.append(assi["Email"].strip())
                if "Email" in item: #check if ther are alternative email adresses
                    altEmail = item["Email"]
                    if ";" in altEmail: #check if there are multiple alternative email adresses
                        listaltEmail = altEmail.split(";")
                        for email in listaltEmail:
                            self.recipients.append(email.strip())
                    else:
                        self.recipients.append(altEmail.strip())

                self.recipients = list(set(self.recipients)) #remove duplicates from recipients list
                self.data_for_excel[item["id"]]["Empfängerinnen"] = self.recipients #add recipients to dict for excel
                
                
                #update data for mail body according to planungsgruppe
                #check Planungsgruppe
                if 'Planungsgruppe' in item and item["Planungsgruppe"]!= "":
                    self.data_for_excel[item["id"]]["Planungsgruppe"] = item["Planungsgruppe"]
                    if item["Planungsgruppe"] in self.planungsgruppen_generated: #check if mail for this planungsgruppe is already generated, if yes --> set already_generated to True
                        self.already_generated= True
                    else:
                        self.already_generated = False     
                        lk_title_PLAN_list = [t[0] for t in self.planungsgruppen_dict[item["Planungsgruppe"]]] #add all LK-titles from this Planungsgruppe 
                        lk_title_PLAN = '/'.join(lk_title_PLAN_list) #join all LK-titles from this Planungsgruppe to a string with "/" as seperater
                        lk_nr_PLAN_list = [t[1] for t in self.planungsgruppen_dict[item["Planungsgruppe"]]] #add all LK-Nummer from this Planungsgruppe 
                        lk_nr_PLAN = '/'.join(lk_nr_PLAN_list)
                        lk_url_PLAN_list = list(set([t[2] for t in self.planungsgruppen_dict[item["Planungsgruppe"]]])) #add all LK-Links from this Planungsgruppe and remove duplicates
                        lk_url_PLAN = '/'.join(lk_url_PLAN_list)
                        try: #try to update data
                            data.update({"lk_title": lk_title_PLAN, "lk_number": lk_nr_PLAN, "date": ExamDate, "exam_link":lk_url_PLAN, "dateMin1w":DateMin1w, "dateMin6w":DateMin6w, "dateMin2w":DateMin2w, "dateMin5w":DateMin5w})
                        except UnboundLocalError as e: #pass if date or exam link do not exist
                            pass
                        self.planungsgruppen_generated.append(item["Planungsgruppe"])
                        subject = "Checklist "+ semester +": " + lk_title_PLAN 
                        filename = LKnummer + "PLAN"
                else:
                    self.data_for_excel[item["id"]]["Planungsgruppe"] = "-"
                    self.already_generated = False 
                    subject = "Checkliste "+ semester +": " + LKTitle 
                    filename = LKnummer 
                    try: #try to update data
                        data.update({"lk_title": LKTitle, "lk_number": LKnummer, "date": ExamDate, "exam_link":ExamLink, "dateMin1w":DateMin1w, "dateMin6w":DateMin6w, "dateMin2w":DateMin2w, "dateMin5w":DateMin5w})
                    except UnboundLocalError as e: #pass if date or exam link do not exist
                        pass
                

                if ExamDate != "" and ExamLink != "": #create mail only if there is a link and a date
                    self.data_for_excel[item["id"]]["Hinweise"] = "" #if this is not set to an empty string, column will no be generated in excel
                    #create Mail Type1, Säule 1
                    if item["Setup"] == "Type1: SEB" or item["Setup"] == "Type1: SEB (SEB-Server)" and item["Saule"]=="1= Zentral":
                        #template = self.env.get_template(r"S:\DES\00_in_Bearbeitung\PythonScript_ChecklistenMails_SZ\inputs\type1.jinja2")
                        #template = self.env.get_template(os.path.join(os.getcwd(), "inputs", "type1.jinja2"))
                        template = self.env.get_template('type1.jinja2')
                        html_content = template.render(data) # Render jinja template
                        filename += "Type1" + date_filename
                        self.__create_msg_file(semester, html_content, self.already_generated, subject, filename, form)
                    
                    #create Mail VDI
                    elif item["Setup"] == "Type2: VDI":
                        #template = self.env.get_template(os.path.join(os.getcwd(), "inputs", "type2.jinja2"))
                        template = self.env.get_template('type2.jinja2')
                        html_content = template.render(data) # Render jinja template
                        filename += "VDI" + date_filename
                        self.__create_msg_file(semester, html_content, self.already_generated, subject, filename, form)

                    #create Mail mobil --> not used anymore!!
                    #elif item["Saule"]=="2= Mobile":
                        #template = self.env.get_template("./inputs/type1_mobile.jinja2")
                        #html_content = template.render(data) # Render jinja template
                        #filename += "Mobil"
                        #self.__create_msg_file(semester, html_content, self.already_generated, subject, filename, form)
                else:
                    self.data_for_excel[item["id"]]["Hinweise"] = "Mail nicht generiert, da kein Prüfungsdatum oder Prüfungslink"

        
        self.generate_excel(form, semester) #generate excel after had looped over every item and filled in the dict self.data_for_excel


    def generate_excel(self, form:str, semester: str):
        """
        Generates an excel file with all exams from self.data_for_excel
        Parameters:
            - form (string): Prüfungsform 
        Returns:
            - excel file: with 9 columns (ID, LK-Nummer, LK-Titel, Hinweise, To do Termine, Exam date, Prüfungslink, Empfängerinnen, Planungsgruppe)
        """
        if self.data_for_excel != {}:
            df = pd.DataFrame.from_dict(self.data_for_excel, orient='index') #create dataframe out of dict
            new_column_order = ["LK-Nummer", "LK-Titel", "Hinweise", "To do Termine", "Exam date", "Prüfungslink", "Empfängerinnen", "Planungsgruppe"] #defin ne column order
            df_orderd = df[new_column_order] #reorder colummns
            now = datetime.now() # Get the current date and time
            current_date_str = now.strftime("%Y-%m-%d") # Format the date as a string
            #filename = current_date_str, form, semester, ".xlsx"
            #filename = "output/" + current_date_str + form + semester + ".xlsx" 
            output_folder = r"S:\DES\00_in_Bearbeitung\PythonScript_ChecklistenMails_SZ\outputs"
            os.makedirs(output_folder, exist_ok=True) #check if there is an output folder and if not --> create one
            filename = os.path.join(output_folder, current_date_str + "_" + form + "_" + semester + ".xlsx")
            df_orderd.to_excel(filename, engine='openpyxl') #write dataframe to excel 
        else: 
            print("CAUTION: No excel was generated: no exams for", form, "in", semester)
        



