import win32com.client as win32
import os

form = "" #adjust this
semester = "" #adjust this


def list_msg_files(folder_path):
    """
    returns a list with all the e-mail drafts from a given folder
    Parameters:
            - folder_path (string): the path to the folder with the mail drafts
    """
    files = os.listdir(folder_path) # List all files in the foler
    msg_files = [file for file in files if file.endswith('.msg')] #make list with the names of all e-mail drafts 
    return msg_files


def send_email_from_msg(semester, form, shared_mail):
    """
    sends all drafts located in a given folder 
    Parameters:
            - folder_path (string): the path to the folder with the mail drafts
            - sharde_mail (string): the mails are sent from the shared mail and the shared mail will also be in the bbc
    """
    # Initialize Outlook
    folder_path = os.path.join(r"S:\DES\00_in_Bearbeitung\PythonScript_ChecklistenMails_SZ\outputs", semester, form)
    outlook = win32.Dispatch('Outlook.Application')
    msg_files = list_msg_files(folder_path) #call function the create list with all the drafts 
    for mes in msg_files: #loop through the list of drafts
        msg = outlook.CreateItemFromTemplate(os.path.join(os.getcwd(), folder_path, mes)) #create mail 
        msg.BCC = shared_mail #define bbc 
        msg.SentOnBehalfOfName = shared_mail #declare, that mails has to be sent from shared mail 
        msg.Send() 
        print("sent the following e-mail: ", mes)

send_email_from_msg(semester, form, shared_mail="digital-exams@id.ethz.ch")
