import msal
import os
import authenticators
import requests

class SPconnection:

    def __init__(self) -> None:
        self.OP_APP_SCOPES = authenticators.OP_APP_SCOPES
        self.OP_APP_CLIENT_ID = authenticators.OP_APP_CLIENT_ID
        self.OP_APP_AUTHORITY = authenticators.OP_APP_AUTHORITY
        self.OP_APP_THUMBPRINT = authenticators.OP_APP_THUMBPRINT
        self.OP_SP_NAME = authenticators.OP_SP_NAME
        self.OP_SITE = authenticators.OP_SITE


    def __get_sp365_token(self, cert_path) -> dict:
        """¨
        generates token for SharePoint 356 access
        Parameters: 
            - string: path to .pem certificate
        Returns: 
            - dict: token for SharePoint 356 access
        """
        app = msal.ConfidentialClientApplication(
            self.OP_APP_CLIENT_ID,
            authority=self.OP_APP_AUTHORITY,
            client_credential={
                'thumbprint': self.OP_APP_THUMBPRINT,
                'private_key': open(cert_path).read()
            })
        #o365_token = app.acquire_token_silent(self.OP_APP_SCOPES, account=None) #check cache for token
        o365_token = app.acquire_token_for_client(scopes=self.OP_APP_SCOPES)
        #if not o365_token: #if no token found in cache
            #o365_token = app.acquire_token_for_client(scopes=self.OP_APP_SCOPES) #get new token
        if "access_token" not in o365_token: #checks if attempt to acquire a token was succesfull 
            print("Could not get token from Microsoft Online")
            exit(1)
        return o365_token
    

    def __get_SP_data(self, cert_path: str, listname: str) -> list:
        """connects to SharePoint with access token and retrieves list
        Paramters:
            - cert_path: string: path to .pem certificate
            - listname: string: SP list from which data should be retrieved
        Returns: 
            - specified SP list with all exams in json format """
        if listname not in ["Anträge", "Portfolio", "Termine", "Archiv"]: #checks if input listname exists 
            raise Exception("Cannot deal with list " + listname)
        #get access token 
        sp_access_token = self.__get_sp365_token(cert_path)
        endpoint = f'https://graph.microsoft.com/v1.0/sites/{self.OP_SP_NAME}:/sites/{self.OP_SITE}'
        #get data via graph API: "Bearer" is a type of token in HTTP authentication.Bearer tokens are commonly used for authentication in OAuth 2.0 and are used to grant access to protected resources.
        graph_data = requests.get(endpoint, headers={'Authorization': 'Bearer ' + sp_access_token['access_token']}).json() 
        #get id to request a certain list
        path_id = graph_data['id'] 
        endpoint_id_termine = f'https://graph.microsoft.com/v1.0/sites/{path_id}/lists/{listname}/items?expand=fields'
        exam_list = []
        while endpoint_id_termine:
            graph_list_data= requests.get(endpoint_id_termine, headers={'Authorization': 'Bearer ' + sp_access_token['access_token']}).json()
            for item in graph_list_data["value"]:
                exam_list.append(item['fields'])
            endpoint_id_termine = graph_list_data.get('@odata.nextLink')  # Follow the pagination link, if present
        return exam_list
    
    def get_data_from_Termine(self) -> list:
        """calls __get_SP_data and fetches data from Termine
        Returns: 
            - specified SP list with all exams in json format """
        termine_data = self.__get_SP_data(authenticators.cert_path, "Termine")
        return termine_data
    
    def get_data_from_Portfolio(self)-> list:
        """calls __get_SP_data and fetches data from Portfolio
        Returns: 
            - specified SP list with all exams in json format """
        portfolio_data = self.__get_SP_data(authenticators.cert_path, "Portfolio")
        return portfolio_data
    
    def get_data_from_Archiv(self)-> list:
        """calls __get_SP_data and fetches data from Archiv
        Returns: 
            - specified SP list with all exams in json format """
        archiv_data = self.__get_SP_data(authenticators.cert_path, "Archiv")
        return archiv_data


#SPConnection = SPconnection().get_data_from_Termine()
