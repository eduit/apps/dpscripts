# ChecklistenMail
1. Generates Checklistenmails with a jinja template and data from Sharepoint (https://ethz.sharepoint.com/sites/online-pruefungen/Lists/Termine/Termine_Complete.aspx) according to the given parameters and stores them in a folder named "output". 
2. Generates an excel file with all the exams according to the given parameters with the following information: 
spID, LK-Nummer, LK-Titel, Hinweise, To do Termine, Exam date, Prüfungslink, Empfängerinnen, Planungsruppe
3. It has also the option to fetch data from Archiv and Portfolio lists additionally to the Termin list. 

## Available Scripts
* ChecklistenMails.py
* run.py
* SP_connection.py
* tests.py

## How to use

1. Adjust the jinja-templates (inputs) according to your use. 
2. Adjust the paramenters "form" and "semester" in `ChecklistenMail(SPdata).generate_mail(semester="FS24", form="Session")` accordingly in the main function in run.py. options: Benotete SL, Session, Semesterend, Semesterend (W)
3. run the run.py Script 

## requirements
#### packages: 
1. create a virtual environment: Create: python -m venv myenv
Activate venv: myenv\Scripts\activate
Deactivate: deactivate

#### authentication: 
authenticators to SharePoint online are needed (.pem file). You can find them here: S:\DES\00_in_Bearbeitung\PythonScript_Auth_SZ
they should be stored in the "ChecklistenMail" dir (I am working on a better solution for this issue)