
from op_tools import op_functions as op, op_cleaner, configuration
import pandas as pd
# What to retrieve from VVZ
vvz_semesters = ['2024W', '2025S'] #which semester do you want to compare? 
hs_fs = "FS" 

config = configuration.read_config()

# ==============================================================================
# Get data
# ==============================================================================

# %% Get Data Portfolio
list_name = "Portfolio"
df_pf = op.sp_get_data(config, list_name)
print(df_pf.columns)
df_pf = op_cleaner.sp_clean_data(df_pf, list_name)


# %% Get Data VVZ lookup
df = df_pf.copy()
df = df[df["Bis"].isin(["present"])]
df = df[df[hs_fs].isin(["Haupt", "Neben"])]
lks = pd.Series(df["LK-Nummer"].unique())  # Only unique LKs
df_vvz = op.vvz_get_data(lks, semesters=vvz_semesters, output_format="sharepoint")  # Check 2022HS, then 2022FS
df_vvz = df_vvz.applymap(lambda x: x.strip(" -") if isinstance(x, str) else x) #strip whitspaces from all the entries 



# ==============================================================================
# Compare data
# ==============================================================================


# Portfolio vs. VVZ
df = df_pf.copy()
df = df[df["Bis"] == "present"]
df = df[df[hs_fs].isin(["Haupt", "Neben"])]
df = df[~df["Prüfungsform"].isin(
    ["Leistungselemente", "formative Prüfung"])]  # VVZ has no information on these types of exams
df["Examinator/In"] = df["Examinator/In"].str.rsplit(" ", expand=True, n=2)[0]  # VVZ data has only lastnames
df = df.applymap(lambda x: x.strip(" -") if isinstance(x, str) else x) #strip whitspaces from all the entries 
op.compare_data(df, df_vvz, "Portfolio_VVZ")