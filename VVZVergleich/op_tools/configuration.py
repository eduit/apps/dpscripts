"""
Manage and evaluate configuration

Obsolete since the move to certificate based login.
"""
import socket
import json
import os
import sys
import getpass
import base64

OP_MAIL_ADDRESS = "digital-exams@id.ethz.ch"
OP_TENANT = "ethz.onmicrosoft.com"
OP_SP_NAME = "ethz.sharepoint.com"
OP_SITE = 'online-pruefungen'
OP_APP_AUTHORITY = f'https://login.microsoftonline.com/{OP_TENANT}'
OP_APP_CLIENT_ID = "a0199f30-3035-404a-b693-e772fc5d456c"
OP_APP_THUMBPRINT = "1FDAE74F8ADF2401E62A2D9370EEDBCD53BE0ED1"
OP_APP_SCOPES = ['https://graph.microsoft.com/.default']

# sharepoint_url = 'https://ethz.sharepoint.com/sites/PRO-AKDI-00000860'
# sharepoint_url = 'https://ethz.sharepoint.com/sites/let-UAT'
sharepoint_url = "https://ethz.sharepoint.com/sites/online-pruefungen"


# Mapping to use a specific configuration file depending on the current execution host of the script
config_file_mapping = {
    "let-006":
        "alex_config.json",
    # CG alter Lappi
    "let-012":
        "C:\\Users\\gcarmen\\.spyder-py3\\OP\\configs_SP\\config_CG_test.json",
    # CG neuer Lappi / the only way that worked was to create the json file on the old laptop
    "let-039":
        "C:\\Users\\gcarmen\\Documents\\python\\config_CG_for_BRs_script.json",
    "let-021":
        "\\\\id-ms-srv-fs01\\mskunden\\SLA128LET\\giannocg\\Desktop\\New folder\\Spyder_config\\ggconfig_file.json",
    "let-017":
        "\\\\id-ms-srv-fs01\\mskunden\\SLA128LET\\shundseder\\Documents\\Python Scripts\\config_CG_test.json",
    "let-037":
        "Config_SH.json"
}


def read_config():
    """
    Moving to certificate based login, the configuration stuff has become obsolete. No more files to read.
    """
    config = {'SP_URL': sharepoint_url}
    return config


def create_config(file_name=""):
    """
    Return and optionally also save a config object that is needed to connect
    to Sharepoint, including login credentials.

    Keyword arguments:
    file_name -- String. If not provided, no file will be created. If provided,
                 must be an absolute ("C:\\config.json" ) or relative path
                 ("config.json"). In the latter case, it will be saved in the
                 current working directory. The created config file can be read
                 with function read_config().
    """
    print("No need to create a configuration file anymore.")
    exit(0)
