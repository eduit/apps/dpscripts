import pandas
import numpy
import os
from datetime import datetime

import pandas as pd
from dateutil import parser
import xlsxwriter


valid_lists = ["Anträge", "Portfolio", "Termine", "Archiv"]
time_cols = ["Raum von", "Raumfreigabe", "Raum bis"]
date_cols = ["Datum", "Beantragt am"]
boolean_cols = ["Kopfhörer", "Nachteilsausgleich", "Change Request"]

# Maps SP on premise names to new SP 365 names by renaming new SP 365 column names back old names
sp_online_column_mappings = {
    'Title': 'LK-Nummer',
    'LKTitle': 'LK-Titel',
    'Assistant': 'Korrespondenz',
    'Assistant2': 'Weitere Assistenten',
    'Beantragt': 'Beantragt am',
    'ChangeRequest': 'Change Request',
    'Email': 'Alternative Email Adresse',
    'Examinator': 'Examinator/In',
    'Examinator2': 'Weitere Examinator/In',
    'Fernpruefung': 'Fernprüfung',
    'Kandidaten': '#Kandidaten',
    'Kommentar': 'Kommentar Antrag Examinator/in',
    'Kommentar0': 'Kommentar',
    'Kopfhorer': 'Kopfhörer',
    'Lehrspezialist': 'Lehrspezialist/In',
    'Pruefungsform': 'Prüfungsform',
    'Pruefungslink': 'Prüfungslink',
    'Pruefungssprache': 'Prüfungssprache',
    'RequestLanguage': 'Request Language',
    'RequestStatus': 'Request Status',
    'Raeume': 'Räume',
    'RaumBis': 'Raum bis',
    'RaumVon': 'Raum von',
    # 'RaumStart': 'Raum von (hh:mm)',
    # 'RaumEnd': 'Raum bis (hh:mm)',
    # 'RecipientsId': 'Recipients',
    'Saule': 'Säule',
    'SendInvite': 'Send invitation',
    'SetupKommentar': 'Setup Kommentar',
    'Software': 'Type1 Software',
    'SupportInfo': 'Support Information',
    # 'SysAdminsId': 'SysAdmins',
    'Termine': '#Termine',
    'TerminStatus': 'Termin Status',
    'ToDo': 'Super To Do',
    'TodoPortfolio': 'AufgabenPortfolio',
    'TodoPortfolioKomm': 'AufgabenKommentarPortfolio',
    'USKeyboards': 'US Keyboards',
    'Vorfaelle': 'Vorfälle',
    'WSetup': 'Wunsch Setup Examinator/In'
}
#
bad_cols = ["Eindeutige ID", 'Maske der effektiven Berechtigungen', "Ebene", 'Geändert', 'Anlagen', 'Name',
            'Elementtyp', 'Erstellt', 'owshiddenversion', 'Genehmigungsstatus', 'Kommentar', 'URL-Pfad', 'index', 'Tag',
            'ServerRedirectedEmbedUri', 'ServerRedirectedEmbedUrl', 'AssistantStringId', 'Assistant2StringId',
            'FileSystemObjectType', 'Modified', 'OData__UIVersionString', 'RecipientsId', 'RecipientsStringId',
            'SendInvite', 'StartCheck', 'SysAdminsStringId', 'oldID', 'Attachments', 'AuthorID', 'ComplianceAssetId',
            'ContentTypeId', 'Created', 'EditorId', 'ExaminatorStringId', 'Examinator2StringId',
            'LehrspezialistStringId', 'AuthorId', 'Id', 'GUID', 'Raumfreigabe_x0020__x0028_hh_x00', 'RaumStart',
            'RaumEnd', '@odata.etag', 'Edit', 'EditorLookupId', 'FolderChildCount', 'ItemChildCount',
            'LehrspezialistLookupId', 'LinkTitle', 'LinkTitleNoMenu', '_ComplianceFlags', '_ComplianceTag',
            '_ComplianceTagUserId', '_ComplianceTagWrittenTime']


def rename_columns(df):
    """
    Rename columns back to SP on premise.

    In SP online, many columns were changed, and not always consistently. Because of many dependencies broken,
    we rename them back to what they used to be.

    To be called just after fetching data from SP online, before any further processing.

    :param df: -- Output of sp_get_data (Pandas dataframe)
    """
    return df.rename(columns=sp_online_column_mappings)


def sp_clean_data(df, list_name):
    """
    Clean data that was retrieved from the online examinations Sharepoint site.

    :param df: -- Output of sp_get_data (Pandas dataframe)
    :param list_name: -- string of the requested sharepoint site, must be "Termine", "Portfolio" or "Archiv" (string)
    """

    def clean_sp_lists(string):
        if type(string) == str:
            string = string.split(";#")
            string = [x for x in string if x not in [""]]  # Remove empty elements
            if len(string) > 1:
                string = "; ".join(string)
            else:
                string = "".join(string)
        return string

    def clean_sp_time(time_string):
        if type(time_string) == str:
            if time_string == "" or time_string == "None":
                return ""
            try:
                d = parser.parse(time_string)
                return d.astimezone().strftime("%H:%M")
            except OSError as e:
                string = "Invalid time"
        return ""

    def clean_sp_date(date_string):
        """
        Change datetime string to something Excel is able to recognize
        """
        if type(date_string) == str:
            if date_string == "None" or date_string == "":
                return ""
            try:
                d = parser.parse(date_string)
                return d.astimezone().strftime("%d.%m.%Y")
            except OSError as e:
                return "Invalid date"
        return ""

    def clean_sp_rooms(rooms):
        if type(rooms) == str:
            # rooms = [room.replace("HG ", "") if room in ["HG E26.1", "HG E26.3", "HG E19", "HG E27", "HG D11", "HG D12", "HG D13"] else room for room in rooms ]
            rooms = rooms.replace(" ", "")
            rooms = rooms.replace(",", ";")
            rooms = rooms.split(";")
            rooms = [("HG" + room) if room in ["E26.1", "E26.3", "E19", "E27", "D11", "D12", "D13"] else room for room
                     in rooms]
            rooms = "; ".join(sorted(rooms))
        return rooms

    def clean_sp_booleans(bool_col):
        """
        Change booleans to yes/no
        """
        df.loc[df[bool_col] == False, bool_col] = 'No'
        df.loc[df[bool_col] == True, bool_col] = 'Yes'

    # Check input
    if list_name not in valid_lists:
        raise Exception("Cannot deal with list '" + list_name + "'")
    if not type(df) is pandas.core.frame.DataFrame:
        raise Exception("Input must be Pandas dataframe.")

    # Remove rows
    # df = df[df["Anbieterdepartement"]!="LET"]
    df = df[df["LK-Nummer"] != "00-0000-00S"]
    # try: 
    #     df = df[df["LK-Nummer"] != "00-0000-00S"]
    # except: 
    #     KeyError
    #     print("LK-Nummer 00-0000-00S does not exist")

    # df = df[df["LK-Nummer"] != "111-111-11"] # Schulung

    # Remove irrelevant columns
    good_cols = list(set(df.columns) - set(bad_cols))
    df = df[good_cols]

    # Clean sharepoint lists
    df = df.applymap(clean_sp_lists)

    # Clean sharepoint date/time
    for col in time_cols:
        if col in df.columns:
            df[col] = df[col].astype(str).replace("nan", "")
            df[[col]] = df[[col]].applymap(clean_sp_time)
    for col in date_cols:
        if col in df.columns:
            df[[col]] = df[[col]].applymap(clean_sp_date)

    # Clean sharepoint rooms
    if set(["Räume"]).issubset(df.columns):
        df["Räume"] = df[["Räume"]].astype(str).applymap(clean_sp_rooms)
        df["Räume"] = df["Räume"].replace("nan", "")

    # Clean sharepoint Prüfungslink
    if set(["Prüfungslink"]).issubset(df.columns):
        df["Prüfungslink"] = df["Prüfungslink"].replace(",.*", "", regex=True)

    if list_name == "Anträge":
        # if "Beantragt am" in df.columns:
        df["Beantragt am"] = df["Beantragt am"].astype(str)
        df['Beantragt am'] = df[['Beantragt am']].applymap(clean_sp_date)

    # Add columns that could be empty and thus missing
    cols = ["Planungsgruppe"]
    for col in cols:
        if col not in df.columns:
            df[col] = ""

    # Remove "\x81" from "Planungsgruppe"
    df["Planungsgruppe"] = df["Planungsgruppe"].replace("\x81", numpy.nan)

    # Sort columns
    cols = ["LK-Nummer", "LK-Titel", 'Examinator/In', "Prüfungsform"]
    cols_time = ["Datum", "Dauer", "Raum Von", "Raumfreigabe", "Raum Bis"]

    # Check if cols_time is a subset of df.columns
    if set(cols_time).issubset(df.columns):
        cols += cols_time

    # Identify missing columns
    missing_cols = [col for col in cols if col not in df.columns]
    if missing_cols:
        print(f"Warning: Missing columns - {missing_cols}")

    # Filter out missing columns from cols
    cols = [col for col in cols if col in df.columns]

    # Sort the rest alphabetically
    cols_other = sorted(list(set(df.columns) - set(cols)))

    # Reindex dataframe
    df = df[cols + cols_other]

    # Sort rows
    if set(["Semester", "Datum", "Raum Von", "Raum Bis", "LK-Nummer", ]).issubset(df.columns):
        df = df.sort_values(["Semester", "Datum", "Raum Von", "Raum Bis", "LK-Nummer"])
    elif set(["LK-Nummer"]).issubset(df.columns):
        df = df.sort_values("LK-Nummer")

    # Clean sharepoint date
    # if set(["Datum"]).issubset(df.columns):
    #     df["Datum"] = df["Datum"].dt.strftime("%d.%m.%Y")
    #     df["Datum"] = df["Datum"].replace("NaT", "")

    # Replace empty strings
    df = df.replace("", numpy.nan)
    df = df.replace(" ", numpy.nan)

    # Fix columns from Boolean to Yes/No strings
    for column in boolean_cols:
        if column in df.columns:
            clean_sp_booleans(column)

    # Rename columns
    df = df.rename(columns={"#Anmeldungen": "N_Anmeldungen"})
    df = df.rename(columns={"#Kandidaten": "N_Kandidaten"})

    # Check duplicates in SP
    checkvars = ["LK-Nummer", "Prüfungsform"]
    # Identify missing checkvars
    missing_checkvars = [checkvar for checkvar in checkvars if checkvar not in df.columns]
    if missing_checkvars:
        print(f"Warning: Missing checkvars - {missing_checkvars}")
    
    # Filter out missing columns from checkvars
    checkvars = [checkvar for checkvar in checkvars if checkvar in df.columns]

    # Proceed only if checkvars is not empty
    if checkvars:
        data_dupl = df[df[checkvars].duplicated(keep=False)]
        if not data_dupl.empty:
            print("There are duplicates in the Sharepoint list:\n" + str(data_dupl[checkvars]))
    else:
        print("No valid columns to check for duplicates.")


        # Backup df
    filename = list_name + "_cleaned_" + datetime.now().strftime("%Y%m%d-%H%M%S") + ".xlsx"
    # TODO select processing of multi line fields
    # writes multi line fields, but needs manual adjustment of cell height and width
    if df.empty:
        print("DataFrame is empty. No data to write.")

    # with pd.ExcelWriter(os.path.join(os.getcwd(), "output", filename), engine="xlsxwriter") as writer:
    #     workbook = writer.book
    #     worksheet = workbook.add_worksheet("Sheet1")
    #     worksheet.set_text_wrap()  # Set text wrap for the default format
    #     df.to_excel(writer, index=False, sheet_name="Sheet1")
        
    with pd.ExcelWriter(os.path.join(os.getcwd(), "output", filename)) as writer:
        writer.book.formats[0].set_text_wrap()
        df.to_excel(writer, index=False, engine="xlsxwriter")
    # # writes only first line of multi line fields
    # df.to_excel(os.path.join(os.getcwd(), "output", filename), index=False)
    print("###########################################################")
    print(filename + " saved to './output'")
    print("###########################################################")

    return df
