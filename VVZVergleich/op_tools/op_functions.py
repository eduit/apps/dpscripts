import requests 
import re
from datetime import datetime
import os
import pandas as pd
import msal
import openpyxl
import time
import numpy as np
from bs4 import BeautifulSoup
from op_tools import configuration, op_cleaner


def unpack_userinfo(data_frame, field_name, index, source_field):
    """
    Convert user dicts to string of user info items.

    Data source is a list of {'LookupValue': <user name>, 'Email': <user mail>}.
    Replace the original with a string of concatenated names: 'Doe John;#Muster Peter',
    or mail addresses
    """
    if field_name not in data_frame.columns:
        return
    if not isinstance(data_frame.at[index, field_name], list):
        return

    users_list = []
    for item in data_frame.at[index, field_name]:
        u = item[source_field]
        if len(u) > 0:
            users_list.append(u)

    if len(users_list) > 0:
        data_frame.at[index, field_name] = ";#".join(users_list)
    else:
        data_frame.at[index, field_name] = ""


def unpack_usernames(data_frame, field_name, index):
    """
    Convert user dicts to string of usernames.

    Data source is a list of {'LookupValue': <user name>, 'Email': <user mail>}.
    """
    unpack_userinfo(data_frame, field_name, index, 'LookupValue')


def unpack_mail_addresses(data_frame, field_name, index):
    """
    Convert user dicts to string of usernames.

    Data source is a list of {'LookupValue': <user name>, 'Email': <user mail>}.
    """
    unpack_userinfo(data_frame, field_name, index, 'Email')


def flatten_list(data_frame, field_name, index):
    """
    Convert list to string, items separated by ';#'.
    """
    if isinstance(data_frame.at[index, field_name], list):
        data_frame.at[index, field_name] = ";#".join(data_frame.at[index, field_name])


def flatten_url(url_dict):
    """
    Convert URL field to string.

    URL fields are dicts {'Desc': 'txt', 'Url': 'https://...'}. Value for 'Url' is returned.
    """
    if isinstance(url_dict, dict):
        return url_dict['Url']
    else:
        return url_dict
    
def get_sp365_token():
    try:
        with open(r"S:\DES\00_in_Bearbeitung\PythonScript_ChecklistenMails_SZ\LETExamAccess2.pem", 'r') as file:
            cert_path = r"S:\DES\00_in_Bearbeitung\PythonScript_ChecklistenMails_SZ\LETExamAccess2.pem"  # path for Windows
    except FileNotFoundError as e:
            cert_path = os.path.join(os.path.dirname(__file__), os.path.pardir, 'LETExamAccess2.pem')
    app = msal.ConfidentialClientApplication(
        configuration.OP_APP_CLIENT_ID,
        authority=configuration.OP_APP_AUTHORITY,
        client_credential={
            'thumbprint': configuration.OP_APP_THUMBPRINT,
            'private_key': open(cert_path).read()
            }
        )

    o365_token = app.acquire_token_silent(configuration.OP_APP_SCOPES, account=None)
    if not o365_token:
        o365_token = app.acquire_token_for_client(scopes=configuration.OP_APP_SCOPES)
        if "access_token" not in o365_token:
            print("Could not get token from Microsoft Online")
            exit(1)
    return o365_token


def sp_get_data(config, list_name):
    """
    Retrieve data from the online examinations Sharepoint site.
    
    Keyword arguments:
    config -- config object (see create_config(), read_config())
    list_name -- string of the requested sharepoint site, must be "Termine", "Portfolio" or "Archiv" (string)
    """
    # Access global variable where token is stored for subsequent use
    sp_access_token = None
    
    # Check input
    if list_name not in ["Anträge", "Portfolio", "Termine", "Archiv"]:
        raise Exception("Cannot deal with list " + list_name)

    # Connect and get data
    if not sp_access_token:
        sp_access_token = get_sp365_token()

    # Get path
    endpoint = f'https://graph.microsoft.com/v1.0/sites/{configuration.OP_SP_NAME}:/sites/{configuration.OP_SITE}'
    graph_data = requests.get(
        endpoint, headers={'Authorization': 'Bearer ' + sp_access_token['access_token']}).json()

    path_id = graph_data['id']

    # Load all list items
    endpoint = f'https://graph.microsoft.com/v1.0/sites/{path_id}/lists/{list_name}/items?expand=fields'
    item_list = []
    while endpoint:
        graph_data = requests.get(
            endpoint, headers={'Authorization': 'Bearer ' + sp_access_token['access_token']}).json()
        for item in graph_data['value']:
            item_list.append(item['fields'])
        # more batches available?
        try:
            endpoint = graph_data["@odata.nextLink"]
        except:
            endpoint = None

    df = pd.DataFrame(item_list)


    # df["ID"] = df["ID"].astype(str).astype(int)
    # df = df.sort_values(by=['ID'])
    df = df.reset_index()

    # Convert names from SP 365 to old SP on premise names
    df = op_cleaner.rename_columns(df)
    
    # Reformat users
    #
    # Change column type for Lehrspezialist/In to string. Otherwise non numeric content (names)
    # will result in NaN.
    # df['Lehrspezialist/In'] = df['Lehrspezialist/In'].astype(str)
    for idx, row in df.iterrows():
        unpack_usernames(df, "Korrespondenz", idx)
        unpack_usernames(df, "Weitere Assistenten", idx)
        unpack_usernames(df, "Examinator/In", idx)
        unpack_usernames(df, "Weitere Examinator/In", idx)
        unpack_usernames(df, "Lehrspezialist/In", idx)
        unpack_usernames(df, "Recipients", idx)
        unpack_usernames(df, "SysAdmins", idx)
        unpack_mail_addresses(df, "Korrespondenz_mail", idx)
        unpack_mail_addresses(df, "Examinator/In_mail", idx)

    # Multi fields are now dicts, convert to strings with items separated by ';#'
    for field in ["Räume", "Type1 Software", "AufgabenPortfolio"]:
        if field in df.columns:
            for idx, row in df.iterrows():
                flatten_list(df, field, idx)

    # URL fields are now dicts {'Desc': 'txt', 'Url': 'https://...'}
    if 'Prüfungslink' in df.columns:
        df['Prüfungslink'] = df['Prüfungslink'].apply(flatten_url)

    # Sort columns alphabetically
    # df = df.reindex(sorted(df.columns), axis=1)

    # Backup df
    filename = list_name + "_" + datetime.now().strftime("%Y%m%d-%H%M%S") + ".xlsx"
    df.to_excel(os.path.join(os.getcwd(), "output", filename), index=False)
    print("###########################################################")   
    print(filename + " saved to './output'")
    print("###########################################################")
    return df


def vvz_get_data(lks, semesters, output_format):
    """
    Search LKs in the Vorlesungsverzeichnis (VVZ). The information is returned 
    as a Pandas dataframe and saved as an XLSX in folder ./output.  

    Keyword arguments:
    df1 -- Output of sp_get_data/sp_clean_data/vvz (Pandas dataframe)
    df2 -- Output of sp_get_data/sp_clean_data/vvz (Pandas dat
    
    ToDo: 
    - deal with basisprüfung
    
    Keyword arguments:
    lks -- List of LKs (Pandas series)
    semesters -- List of semesters that will searched (e.g., ["2019S", "2018W"])
    output_format -- 'raw' or 'sharepoint' (string).
                      'raw': Return all information
                      'sharepoint': Return a selection of information formatted
                                    similar to the Sharepoint site (see sp_get_data())
    """

    # Check input
    if not type(lks) is pd.core.frame.Series:
        raise Warning("lks must be Pandas Series.")
    if output_format not in ["raw", "sharepoint"]:
        raise Warning("Cannot deal with output_format '" + output_format + "'")  
        
    # URL VVZ
    url1 = "http://www.vvz.ethz.ch/Vorlesungsverzeichnis/sucheLehrangebot.view"
    url2 = 'http://www.vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view'
    
    # Remove S/J/L
    # lks_clean = lks.str.replace("[a-zA-Z]", "", regex=True)
    lks_clean = lks.str[:-1]  # Remove last character
    
    # Prepare df_raw
    df_raw = pd.DataFrame(columns=['lk', 'col', 'val'])
   
    for i in range(len(lks_clean)):
        try:
            
            this_lk_clean = lks_clean.iloc[i]
            this_lk = lks.iloc[i]
            
            print("###########################################################")   
            print(this_lk_clean)
            for this_semkez in semesters:
                form_data = {
                    'lerneinheitscode': this_lk_clean,
                    'semkez': this_semkez
                }
                
                r = requests.get(url1, params=form_data)
                time.sleep(0.1)
                    
                ids = []
                for m in re.finditer('lerneinheitId=[0-9]*&semkez', r.text):       
                    ids.append(r.text[m.start()+14: m.end()-7])
                ids = list(set(ids))
                
                if len(ids) > 1:
                    raise Warning("I cannot deal with multiple IDs atm. Skipping it...")
                elif len(ids) == 1:
                    break
                elif len(ids) == 0 and this_semkez == semesters[-1]:
                    raise Warning("LK " + this_lk_clean + " could not be found. Skipping it...")
        
            for this_id in ids:  
                # this_id = ids[0]
                
                # Katalogdaten
                form_data = {
                    'lerneinheitId': this_id,
                    'semkez': this_semkez,
                    'ansicht': 'KATALOGDATEN'
                }    
            
                r = requests.get(url2, params=form_data)
                time.sleep(0.1)
                tab1 = pd.read_html(r.text, flavor='html5lib')[0]
                tab2 = pd.read_html(r.text, flavor='html5lib')[1]
                if len(tab2.columns) == 3: # debugging CG / GG due to change in VVZ, 17.06.2022
                    tab2 = tab2.drop([2], axis=1) # debugging CG / GG due to change in VVZ, 17.06.2022
            
                # Leistungskontrolle
                form_data['ansicht'] = 'LEISTUNGSKONTROLLE'
                r = requests.get(url2, params=form_data)
                time.sleep(0.1)
                tab3 = pd.read_html(r.text, flavor='html5lib')[1]
                
                index_j = tab3[tab3[0].str.contains('Jahreskurs') == True]
                index_s = tab3[tab3[0].str.contains('Semesterkurs') == True]
                if not index_j.empty and not index_s.empty:
                    if this_lk[-1] == "J":
                        tab3 = tab3.iloc[:index_s.index[0]]
                    if this_lk[-1] == "S":
                        tab3 = tab3.iloc[index_s.index[0]:]
                    else:
                        tab3 = tab3.iloc[:index_s.index[0]]                    # Take Jahreskurs
                
                # Combine
                tab = pd.concat((tab1, tab2, tab3))
                tab.columns = ["col", "val"]
                
                # Add LK-Titel
                soup = BeautifulSoup(r.text, 'html.parser')
                h1 = str(soup.select("h1")[0])
                m = re.search('<h1>.* ', h1)

                tab = pd.concat((tab, pd.DataFrame({"col": ["LK-Titel"], "val": [h1[m.end()+8:len(h1)-5]]})))
                tab["lk"] = this_lk # Uncleaned LK
    
                # Add to df_out
                df_raw = pd.concat((df_raw, tab))

            # Sort columns
            df_raw = df_raw[["lk", "col", "val"]]
            
        except Exception as e:
            print(e)
            continue
        
    if output_format == "raw":
        df_out = df_raw
    elif output_format == "sharepoint":
        # Select cols

        cols = ["Semester", "Periodizität", "Prüfende", "Form", "Repetition", "Prüfungsmodus", "Zusatzinformation zum Prüfungsmodus", "LK-Titel", "Im Prüfungsblock für"]
        df_sp = df_raw[df_raw["col"].isin(cols)]

        # Reshape
        df_sp = df_sp.pivot(index='lk', columns='col', values='val')
        df_sp = df_sp.reset_index(level=0)

        # Add "Im Prüfungsblock für" if missing
        if not ("Im Prüfungsblock für" in df_sp.columns):
            df_sp["Im Prüfungsblock für"] = np.nan
            
        # Rename 
        df_sp = df_sp.rename(columns={'lk': 'LK-Nummer'})

        # Loop through rows
        for idx, row in df_sp.iterrows():
            # Form
            df_sp.loc[idx, "Prüfungsform"] = row[["Form"]].replace(["benotete Semesterleistung", "unbenotete Semesterleistung", "Semesterendprüfung", "Sessionsprüfung"],
                                      ["Benotete SL", "Unbenotete SL", "Semesterend", "Session"]).iat[0]

            # Semester
            if row["Semester"].find("Frühjahrssemester") > -1:
                sem = "FS"
            else: 
                sem = "HS"
            df_sp.loc[idx, "Semester"] = row["Semester"][-4:] + sem

            if str(row["Im Prüfungsblock für"]).find("(Basisprüfung)") != -1:
                is_bp = True
            else:
                is_bp = False
                
            if row["Repetition"] == "Die Leistungskontrolle wird nur am Semesterende nach der Lerneinheit angeboten. Die Repetition ist nur nach erneuter Belegung möglich.":
                haupt_exam = "yes"
                neben_exam = "no"
            if row["Repetition"] == "Es wird ein Repetitionstermin in den ersten zwei Wochen des unmittelbar nachfolgenden Semesters angeboten.":
                haupt_exam = "yes"
                neben_exam = "no"                    
            if row["Repetition"] == "Die Leistungskontrolle wird in jeder Session angeboten. Die Repetition ist ohne erneute Belegung der Lerneinheit möglich.":
                haupt_exam = "yes"
                neben_exam = "yes"     
            if row["Repetition"] == "Die Leistungskontrolle wird nur in der Session nach der Lerneinheit angeboten. Die Repetition ist nur nach erneuter Belegung möglich.":
                haupt_exam = "yes"
                neben_exam = "no"                       
            if row["Repetition"] == "Repetition nur nach erneuter Belegung der Lerneinheit möglich.":
                haupt_exam = "yes"
                neben_exam = "no"
            if row["Repetition"] == "Repetition ohne erneute Belegung der Lerneinheit möglich.":
                haupt_exam = "yes"
                neben_exam = "no"                    
                
            if sem == "HS":
                if haupt_exam == "yes" and is_bp:
                    FS = "Haupt"
                elif haupt_exam == "yes" and not is_bp:
                    HS = "Haupt"
                elif haupt_exam == "no":
                    HS = "Kein"

                if neben_exam == "yes" and is_bp:
                    HS = "Neben"
                elif neben_exam == "yes" and not is_bp:
                    FS = "Neben"
                elif neben_exam == "no":
                    FS = "Kein"
            if sem == "FS":
                if haupt_exam == "yes" and is_bp:
                    HS = "Haupt"
                elif haupt_exam == "yes" and not is_bp:
                    FS = "Haupt"
                elif haupt_exam == "no":
                    FS = "Kein"

                if neben_exam == "yes" and is_bp:
                    FS = "Neben"
                elif neben_exam == "yes" and not is_bp:
                    HS = "Neben"
                elif neben_exam == "no":
                    HS = "Kein"

            df_sp.loc[idx, "FS"] = FS
            df_sp.loc[idx, "HS"] = HS
            
            # Prüfende
            if not pd.isna(row["Prüfende"]):
                row["Prüfende"] = row["Prüfende"].replace('\xa0', u'')
                row["Prüfende"] = re.sub(r"[A-Z]\.", "", row["Prüfende"])
                row["Prüfende"] = row["Prüfende"].replace(' ', '')
                df_sp.loc[idx, "Examinator/In"] = row["Prüfende"].split(",")[0]

            # Prüfungsmodus
            if "Prüfungsmodus" in row.index:
                df_sp.loc[idx, "Dauer"] = "".join(re.findall('\d+', str(row["Prüfungsmodus"]) ))

            # Zusatzinformation zum Prüfungsmodus -> veraltet, weil wir's mittlerweile via Sempro "Aktiv" = "Ja" Flag steuern
            df_sp.loc[idx, "VVZ_Eintrag"] = "Nein"  
            if "Zusatzinformation zum Prüfungsmodus" in row.index:
                if str(row["Zusatzinformation zum Prüfungsmodus"]).lower().find("computer") > -1:
                    df_sp.loc[idx, "VVZ_Eintrag"] = "Ja"
                elif str(row["Zusatzinformation zum Prüfungsmodus"]).lower().find("pc") > -1:
                    df_sp.loc[idx, "VVZ_Eintrag"] = "Ja"   
    
        # Select columns
        df_out = df_sp[["LK-Nummer", "LK-Titel", "Semester", "Examinator/In", "Prüfungsform", "FS", "HS", "Dauer", "VVZ_Eintrag"]]
    
    # Write XLSX
    filename = "vvz-"+datetime.now().strftime("%Y%m%d-%H%M%S")+".xlsx"
    df_out.to_excel(os.path.join("output", filename), index=False)
    print("###########################################################")   
    print(filename + " saved to './output'")
    print("###########################################################")
   
    return df_out


def compare_data(df1, df2, filename_extension=""):
    """
    Compare two data sources, which are matched on columns "LK-Nummer" and
    "Prüfungsform". The matched dataframe is returned and saved as an XLSX
    in folder ./output.  

    Keyword arguments:
    df1 -- Output of sp_get_data/sp_clean_data/vvz containing columns 'Title' and 'Prüfungsform' (Pandas dataframe)
    df2 -- Output of sp_get_data/sp_clean_data/vvz containing columns 'Title' and 'Prüfungsform' (Pandas dataframe)

    'Title' had been 'LK-Nummer' and was stupidly renamed in Sharepoint 365
    """       
            
    # Check input
    if not type(df1) is pd.core.frame.DataFrame:
        raise Warning("df1 must be Pandas dataframe.")  
    if not type(df2) is pd.core.frame.DataFrame:
        raise Warning("df2 must be Pandas dataframe.")  
        
    cols = ["LK-Nummer", "Prüfungsform"]
    for col in cols:
        if col not in df1.columns:
            raise Warning("Column '" + col + "' is missing in df1")
        if col not in df2.columns:
            raise Warning("Column '" + col + "' is missing in df2")
           
    # Select common columns
    common_cols = list(set(df1.columns) & set(df2.columns))
    df1 = df1[common_cols]
    df2 = df2[common_cols]
    
    # Remove irrelevant columns
    cols = ["Eindeutige ID", "Id", "Maske der effektiven Berechtigungen", "Ebene", "Geändert", "Anlagen", "Name", "Elementtyp",
            "Erstellt", "owshiddenversion", "Genehmigungsstatus", "Kommentar0", "URL-Pfad", "index"]
    
    good_cols = list(set(df1.columns) - set(cols))
    df1 = df1[good_cols]
    df2 = df2[good_cols]

    # Wide to long
    match_cols = ["LK-Nummer", "Prüfungsform"]
    df1l = pd.melt(df1, id_vars=match_cols,
                   var_name="var", value_name="val")
    df2l = pd.melt(df2, id_vars=match_cols,
                   var_name="var", value_name="val")
   
    # Merge
    df_match = pd.merge(df1l, df2l, how="outer", on=match_cols + ["var"], suffixes=["1", "2"], indicator=True)
    df_match = df_match.replace(["both", "left_only", "right_only"], ["both", "Portfolio_only", "VVZ_only"])
        
    # Compare                   
    df_match["val1_format"] = df_match["val1"]
    df_match["val2_format"] = df_match["val2"]
    df_match["desc"] = ""
    for idx, row in df_match.iterrows():   
       
        # Skip if non-matching LK
        if row["_merge"] != "both":
            df_match.loc[idx, "desc"] = row["_merge"]
            continue

        # Go to next when both are empty
        if ( (row["val1_format"] == "" and row["val2_format"] == "") or
              (pd.isna(row["val1_format"]) and pd.isna(row["val2_format"])) ):
            df_match.loc[idx, "desc"] = "no change"
            continue           
        
        # Go to next when one is empty
        if ((row["val1_format"] == "" and row["val2_format"] != "") and
           (row["val1_format"] != "" and row["val2_format"] == "")):
            df_match.loc[idx, "desc"] = "change"
            continue           

        # Force to float if possible
        if str(row["val1_format"]).replace('.', '', 1).isdigit() and str(row["val2_format"]).replace('.', '', 1).isdigit():
            row["val1_format"] = float(row["val1_format"])
            row["val2_format"] = float(row["val2_format"])
            
        # Exact match
        if row["val1_format"] == row["val2_format"]:
            row["desc"] = "no change"
        # Digits: Relative difference
        elif (isinstance(row["val1_format"], (int, float) ) and
              isinstance(row["val2_format"], (int, float) )):
            if row["val1_format"] != 0:
                row["desc"] = "change: " + str( round( abs( (row["val1_format"] - row["val2_format"]) / row["val1_format"]*100), 2 ) ) + "% difference"
            else:
                row["desc"] = "change"
        else:
            row["desc"] = "change"

        # Set status/select
        df_match.loc[idx, "val1_format"] = row["val1_format"]
        df_match.loc[idx, "val2_format"] = row["val2_format"]
        df_match.loc[idx, "desc"] = row["desc"]

    # Add "filter" variable (LK with at least one change)
    df_changes = df_match[ df_match["desc"] != "no change"][["LK-Nummer", "Prüfungsform"]].drop_duplicates()
    df_changes["filter"] = 1
    df_match = pd.merge(df_match, df_changes, how="outer", on=["LK-Nummer", "Prüfungsform"])
    df_match["filter"] = df_match["filter"].fillna(0)

    # Remove columns
    df_match = df_match.drop(["_merge"], axis=1)
    df_match = df_match.sort_values(by=["LK-Nummer", "Prüfungsform", "desc"])
    df_renamed = df_match = df_match.rename(columns={'val1': 'Portfolio', 'val2': 'VVZ', "desc": "Diskrepanz"})
    df_final = df_renamed.drop(columns=['val1_format', "val2_format", "filter"])
    
    # Write XLSX
    filename = "compare-"+filename_extension+"-"+datetime.now().strftime("%Y%m%d-%H%M%S")+".xlsx"
    df_final.to_excel(os.path.join("output", filename), index=False)
    print("###########################################################")   
    print(filename + " saved to './output'")
    print("###########################################################")