# ChecklistenMail
Reworked old Scripts from Bruno (op_tools)

## Available Scripts
* configuration.py
* op_cleaner.py
* op_functions.py
* VVZVergleich.py

## How to use

1. Adjust parameters for the semesters you want to compare in VVZVergleich.py
2. run the VVZVergleich.py Script 

## requirements
#### python version 3.10 is needed 

with python:  
1. download Python version 3.10 and include into PATH
2. create a virtual environment with python 3.10: `path_to_python -m venv myenv`
3. Activate venv: `myenv\Scripts\activate`
4. install packages: `pip install -r requirements.txt`
5. Deactivate: `deactivate`

with Conda (and mini-forge): 
1. create virtual environment: `conda create -n myenv python=3.10`
2. Activate venv: `conda activate myenv`
3. install packages: `conda install --file requirements.txt`
4. Deactivate: `conda deactivate`
